

class Validations {
  static bool validateName(String text) {
    return text.contains(RegExp(r"^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$"));
  }

  static bool validatePhoneNumber(String text) {
    const pattern = r'^[0-9]{9}$';
    final regex = RegExp(pattern);
    return regex.hasMatch(text);
  }

  static bool validateEmail(String text) {
    const pattern = r'^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$';
    final regex = RegExp(pattern);
    return regex.hasMatch(text);
  }

  static bool validatePin(String text) {
    return text
        .toString()
        .length == 6;
  }
}

