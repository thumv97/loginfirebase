import 'package:acazia_training/models/users.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
class BaseFireStore {

  static void addUser(Users user){
     checkUserExist(user.userId).then((value) {
      if (!value) {
        debugPrint('${user.userId} added');
        Firestore.instance
            .document('users/${user.userId}')
            .setData(user.toJson());
      } else {
        debugPrint('user ${user.userId} exists');
      }
    });
  }

  static Future<bool> checkUserExist(String userId)  {
    var exists = false;
    try {
        Firestore.instance.document('users/$userId').get().then((doc) {
        if (doc.exists) {
          exists = true;
        } else {
          exists = false;
        }
        return exists;
      });
    } catch (e) {
      debugPrint(e);
    }
    return null;
  }
  static Stream<Users> getUser(String userId) {
    return Firestore.instance
        .collection('users')
        .where('userId', isEqualTo: userId)
        .snapshots()
        .map((QuerySnapshot snapshot) {
      return snapshot.documents.map((doc) {
        return Users.fromDocument(doc);
      }).first;
    });
  }
}