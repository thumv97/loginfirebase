import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class BaseAuth {

  static Future<String> submitOTP(String verificationId, String smsCode) async {
    final credential = PhoneAuthProvider.getCredential(
      verificationId: verificationId,
      smsCode: smsCode,
    );
    final user =
        (await FirebaseAuth.instance.signInWithCredential(credential)).user;
    return user.uid;
  }

  static Future<String> signUp(String email) async {
    final user = (await FirebaseAuth.instance
            .createUserWithEmailAndPassword(email: email, password: '123456'))
        .user;
    debugPrint('current uid: ${user.uid}');
    return user.uid;
  }

  static Future<String> signIn(String email) async {
    final user = (await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: email, password: '123456'))
        .user;
    return user.uid;
  }

  static Future<String> signInWithFacebook(String accessToken) async {
    final credential = FacebookAuthProvider.getCredential(
      accessToken: accessToken,
    );
    final user =
        (await FirebaseAuth.instance.signInWithCredential(credential)).user;
    return user.uid;
  }

  static Future<FirebaseUser> getCurrentFirebaseUser() async {
    final user = await FirebaseAuth.instance.currentUser();
    return user;
  }

  static void signOut() async {
    debugPrint('signout');
    return FirebaseAuth.instance.signOut();
  }
}
