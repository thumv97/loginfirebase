import 'package:acazia_training/navigation/navigation.dart';
import 'package:acazia_training/res/colors.dart';
import 'package:flutter/material.dart';

Widget backButton() {
  return InkWell(
    onTap: () {
      Navigation.instance.goBack();
    },
    child: Icon(Icons.arrow_back_ios, color: WHITE),
  );
}