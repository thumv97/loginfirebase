import 'package:acazia_training/res/texts.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final String hintText;
  final String labelText;
  final TextEditingController controller;
  final Widget prefixIcon;
  final TextInputType inputType;
  final Function validator;
  final Function onChanged;

  const CustomTextField(
      {Key key,
      this.hintText,
      this.labelText,
      this.controller,
        this.prefixIcon,
      this.inputType,
      this.validator,
      this.onChanged,
      })
      : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: widget.controller,
      style: TEXT_FONT,
      keyboardType: widget.inputType,
      onChanged: widget.onChanged,
      decoration: InputDecoration(
        prefixIcon: widget.prefixIcon,
          enabledBorder: UnderlineInputBorder(
            borderSide:
            BorderSide(color: Colors.white.withOpacity(.2)),
          ),
          labelText: widget.labelText,
          labelStyle: TEXT_FONT,
          hintStyle: TEXT_FONT,
          hintText: widget.hintText),
    );
  }
}
