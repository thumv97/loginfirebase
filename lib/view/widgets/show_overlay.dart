import 'package:acazia_training/res/colors.dart';
import 'package:flutter/material.dart';

Future showOverlay(String textOverlay,BuildContext context) async {
    final overlayState = Overlay.of(context);
    final overlayEntry = OverlayEntry(
        builder: (context) => Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: Container(
            alignment: Alignment.bottomCenter,
            padding: const EdgeInsets.only(bottom: 10),
            height: 80,
            color: const Color(0xFFED6348),
            child: Text(
              textOverlay,
              style: TextStyle(
                fontSize: 16,
                decoration: TextDecoration.none,
                color: WHITE,
              ),
            ),
          ),
        ));
    overlayState.insert(overlayEntry);
    await Future.delayed(const Duration(seconds: 2));
    overlayEntry.remove();
  }

