import 'package:flutter/material.dart';
class CustomButton extends StatefulWidget {
  final Color buttonColor;
  final Color iconColor;
  final VoidCallback onPressed;

   CustomButton({Key key,
    this.buttonColor,
    this.iconColor,
    this.onPressed}) : super(key: key);

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 60,
      height: 60,
      decoration: BoxDecoration(
        color: widget.buttonColor,
        borderRadius: BorderRadius.circular(100.0),
      ),
      child: IconButton(
        icon: Icon(
          Icons.arrow_forward,
          color: widget.iconColor,
        ),
        onPressed: widget.onPressed,
      ),
    );
  }
}
