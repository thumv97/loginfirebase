import 'package:acazia_training/models/users.dart';
import 'package:acazia_training/navigation/navigation.dart';
import 'package:acazia_training/navigation/paths.dart';
import 'package:acazia_training/res/colors.dart';
import 'package:acazia_training/res/texts.dart';
import 'package:acazia_training/service/auth.dart';
import 'package:acazia_training/service/firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';



class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.user}) : super(key: key);
  final FirebaseUser user;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_BACKGROUND,
      body: StreamBuilder(
        stream: BaseFireStore.getUser(widget.user.uid),
        builder: (BuildContext context, AsyncSnapshot<Users> snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(
              ),
            );
          } else {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 100.0,
                    width: 100.0,
                    child: CircleAvatar(
                      backgroundImage: (snapshot.data.profilePictureURL != '')
                          ? NetworkImage(snapshot.data.profilePictureURL)
                          : const AssetImage('assets/images/bg_fb.jpg'),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text('UID: ${snapshot.data.userId}', style: TEXT_FONT),
                  Text(
                    'Name: ${snapshot.data.name}',
                    style: TEXT_FONT,
                  ),
                  Text('Email: ${snapshot.data.email}', style: TEXT_FONT),
                  Text('Phone: ${snapshot.data.phone ?? 'No phone'}',
                      style: TEXT_FONT),

                  Padding(
                    padding: const EdgeInsets.only(top: 100),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        FlatButton(
                          onPressed: () {
                            BaseAuth.signOut();
                            Navigation.instance.goBack();
                          },
                          child: const Text(
                            'SignOut',
                            style: TextStyle(color: WHITE, fontSize: 16),
                          ),
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigation.instance
                                .navigateTo(SIGNUP, arguments: null);
                          },
                          child: const Text('SignUp',
                              style: TextStyle(color: WHITE, fontSize: 16)),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          }
        },
      ),
    );
  }
}
