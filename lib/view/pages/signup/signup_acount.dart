import 'package:acazia_training/models/users.dart';
import 'package:acazia_training/navigation/navigation.dart';
import 'package:acazia_training/navigation/paths.dart';
import 'package:acazia_training/res/colors.dart';
import 'package:acazia_training/res/texts.dart';
import 'package:acazia_training/service/auth.dart';
import 'package:acazia_training/service/firestore.dart';
import 'package:acazia_training/utils/validations.dart';
import 'package:acazia_training/view/widgets/back_button.dart';
import 'package:acazia_training/view/widgets/custom_button.dart';
import 'package:acazia_training/view/widgets/custom_textfield.dart';
import 'package:acazia_training/view/widgets/show_overlay.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SignUpEmailPage extends StatefulWidget {
  final FirebaseUser user;

  const SignUpEmailPage({Key key, this.user}) : super(key: key);

  @override
  _SignUpEmailPageState createState() => _SignUpEmailPageState();
}

class _SignUpEmailPageState extends State<SignUpEmailPage> {
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  bool isVisible = false;
  void _signUpEmail(String name, String phone, String email) {
    BaseAuth.signUp(email).then((uid) {
      BaseFireStore.addUser(
        Users(
            userId: uid,
            email: email,
            phone: phone,
            name: name,
            profilePictureURL: ''),
      );
      Navigation.instance.navigateTo(SIGNUP_CREATED, arguments: null);
    }).catchError((error) {
      showOverlay('Email already exist', context);
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_BACKGROUND,
      body: SingleChildScrollView(
        child: Padding(
          padding: MediaQuery.of(context).padding.add(
              const EdgeInsets.symmetric(vertical: 16.0, horizontal: 10.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              backButton(),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 35, horizontal: 16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const Text(
                        'Create\nAccount',
                        style: TEXT_TITLE,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 30),
                        child: CustomTextField(
                          controller: _nameController,
                          inputType: TextInputType.text,
                          labelText: 'Name',
                          onChanged: (value) {
                            if (!Validations.validatePhoneNumber(value) ||
                                value.toString().isEmpty) {
                              setState(() {
                                isVisible = false;
                              });
                              debugPrint('Invalid');
                            } else {
                              setState(() {
                                isVisible = true;
                              });
                              debugPrint('Correct');
                            }
                          },
                        ),
                      ),
                      Expanded(
                        child: SizedBox(
                          child: CustomTextField(
                            controller: _emailController,
                            inputType: TextInputType.text,
                            labelText: 'Email',
                            onChanged: (value) {
                              if (!Validations.validateEmail(value) ||
                                  value.toString().isEmpty) {
                                setState(() {
                                  isVisible = false;
                                });
                                debugPrint('Invalid');
                              } else {
                                setState(() {
                                  isVisible = true;
                                });
                                debugPrint('Correct');
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Center(
                child: RichText(
                  text: TextSpan(
                    text: 'By signing up. You agree to the ',
                    style: TEXT_FONT,
                    children: <TextSpan>[
                      TextSpan(
                          text: 'Terms\nand Conditions',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                          )),
                      const TextSpan(
                        text: ' and the ',
                      ),
                      TextSpan(
                        text: 'Privacy Policy.',
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      // can add more TextSpans here...
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    CustomButton(
                      buttonColor: isVisible ? WHITE : COLOR_BUTTON_1,
                      iconColor: isVisible ? COLOR_BACKGROUND : WHITE,
                      onPressed: () {
                        isVisible
                            ? _signUpEmail(_nameController.text,
                                widget.user.phoneNumber, _emailController.text)
                            : showOverlay('Invalid Email', context);
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
