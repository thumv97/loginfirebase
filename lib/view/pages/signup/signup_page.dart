import 'package:acazia_training/models/users.dart';
import 'package:acazia_training/navigation/navigation.dart';
import 'package:acazia_training/navigation/paths.dart';
import 'package:acazia_training/res/colors.dart';
import 'package:acazia_training/res/images.dart';
import 'package:acazia_training/res/texts.dart';
import 'package:acazia_training/service/auth.dart';
import 'package:acazia_training/service/firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class SignUpPage extends StatefulWidget {
  @override


  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  static final FacebookLogin facebookSignIn = FacebookLogin();

  void _loginWithFacebook() async {
    final result = await facebookSignIn.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        await BaseAuth.signInWithFacebook(result.accessToken.token).then((uid) {
          BaseAuth.getCurrentFirebaseUser().then((user) {
            if (user != null) {
              final users = Users(
                name: user.displayName,
                userId: user.uid,
                email: user.email,
                phone: '0361763863',
                profilePictureURL: user.photoUrl,
              );
              debugPrint(user.photoUrl);
              BaseFireStore.addUser(users);
              Navigation.instance.navigateTo(HOME, arguments: user);
            }
          });
        });
        break;
      case FacebookLoginStatus.cancelledByUser:
        debugPrint('Login cancelled by the user.');
        break;
      case FacebookLoginStatus.error:
        debugPrint('Something went wrong with the login process.\n'
            'Here\'s the error Facebook gave us: ${result.errorMessage}');
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_BACKGROUND,
      body: Padding(
        padding: const EdgeInsets.fromLTRB(40, 0, 40, 40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              'Sign Up',
              style: TEXT_TITLE,
            ),
            const SizedBox(
              height: 50.0,
            ),
            InkWell(
              onTap: _loginWithFacebook,
              child: Container(
                height: 50.0,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: WHITE,
                  borderRadius: BorderRadius.circular(
                    30.0,
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image(
                      image: AssetImage(ICON_FB),
                    ),
                    const SizedBox(
                      width: 10.0,
                    ),
                    const Text(
                      'Welcome in with FaceBook',
                      style: TextStyle(
                        color: Color(0xFF43619C),
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 15.0,
            ),
            InkWell(
              onTap: () {
                Navigation.instance.navigateTo(SIGNUP_PHONE, arguments: null);
              },
              child: Container(
                height: 50.0,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: WHITE, width: 1),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child: Text(
                  'Start with Phone Number',
                  style: TEXT_FONT,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Joined us before? ',
                  style: TEXT_FONT,
                ),
                InkWell(
                  onTap: () {
                    Navigation.instance
                        .navigateTo(SIGNINPAGE, arguments: null);
                  },
                  child: Text(
                    'Login',
                    style: TEXT_FONT,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
