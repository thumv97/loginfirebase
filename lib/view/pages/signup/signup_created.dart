import 'package:acazia_training/navigation/navigation.dart';
import 'package:acazia_training/navigation/paths.dart';
import 'package:acazia_training/res/colors.dart';
import 'package:acazia_training/res/texts.dart';
import 'package:flutter/material.dart';

class SignUpCreated extends StatefulWidget {
  @override
  _SignUpCreatedState createState() => _SignUpCreatedState();
}

class _SignUpCreatedState extends State<SignUpCreated> {
  @override
  void initState() {
    super.initState();
    _signIn();
  }

  Future<void> _signIn() async {
    await Future.delayed(const Duration(seconds: 2),
        () => Navigation.instance.navigateTo(SIGNINPAGE,arguments: null));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_BACKGROUND,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Image(
              image: AssetImage('assets/images/done_2.png'),
            ),
            const SizedBox(
              height: 50,
            ),
            Text(
              'Profile Created!',
              style: TEXT_TITLE,
            ),
          ],
        ),
      ),
    );
  }
}
