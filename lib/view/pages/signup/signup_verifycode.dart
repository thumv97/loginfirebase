import 'dart:async';
import 'package:acazia_training/navigation/navigation.dart';
import 'package:acazia_training/navigation/paths.dart';
import 'package:acazia_training/res/colors.dart';
import 'package:acazia_training/res/texts.dart';
import 'package:acazia_training/service/auth.dart';
import 'package:acazia_training/utils/validations.dart';
import 'package:acazia_training/view/widgets/back_button.dart';
import 'package:acazia_training/view/widgets/custom_button.dart';
import 'package:acazia_training/view/widgets/show_overlay.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:pin_input_text_field/pin_input_text_field.dart';

class SignUpVerify extends StatefulWidget {
  SignUpVerify({Key key, this.phoneNumberVerify}) : super(key: key);
  final String phoneNumberVerify;
  @override
  _SignUpVerifyState createState() => _SignUpVerifyState();
}

class _SignUpVerifyState extends State<SignUpVerify> {
  final _pinEditingController = TextEditingController();
  bool isVisible = false;
  String verificationId;
  Timer _timer;
  int _start = 30;
  void startTimer() {
    debugPrint('Start time');
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start < 1) {
            showOverlay('Resend Code', context);
            timer.cancel();
          } else {
            _start = _start - 1;
          }
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    startTimer();
    verifyCode();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void verifyCode() async {
    final PhoneVerificationCompleted verificationCompleted =
        (AuthCredential credential) async {
      final user =
          (await FirebaseAuth.instance.signInWithCredential(credential)).user;
      if (user != null) {
        debugPrint('Successfully signed completed, uid: ${user.uid}');
        await Navigation.instance.navigateAndRemove(HOME, arguments: user);
      } else {
        debugPrint('Sign in failed');
      }
    };
    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      debugPrint('Phone number verification failed.'
          'Message: ${authException.message}');
    };
    final PhoneCodeSent codeSent =
        (String verId, [int forceResendingToken]) async {
      verificationId = verId;
      debugPrint('verifyid: $verificationId');
      debugPrint('OTP sent your phone');
    };
    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verId) {
      debugPrint('Resend code');
      verificationId = verId;
    };
    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: '+84${widget.phoneNumberVerify}',
        timeout: const Duration(milliseconds: 30000),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_BACKGROUND,
      body: Padding(
        padding: MediaQuery.of(context)
            .padding
            .add(const EdgeInsets.symmetric(vertical: 16.0, horizontal: 10.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            backButton(),
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 35, horizontal: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Verfication\nCode',
                      style: TEXT_TITLE,
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      'Enter the 6 digit code send to',
                      style: TEXT_FONT,
                    ),
                    Text(
                      '+84${widget.phoneNumberVerify}',
                      style: TEXT_FONT,
                    ),
                    const Spacer(),
                    PinInputTextField(
                      controller: _pinEditingController,
                      keyboardType: TextInputType.phone,
                      pinLength: 6,
                      enabled: true,
                      decoration: UnderlineDecoration(
                          enteredColor: Colors.white30,
                          textStyle: TextStyle(fontSize: 30, color: WHITE),
                          color: Colors.white30),
                      onChanged: (pin) {
                        setState(() {
                          if (Validations.validatePin(pin)) {
                            isVisible = true;
                          } else {
                            isVisible = false;
                          }
                        });
                      },
                    ),
                    const Spacer(),
                  ],
                ),
              ),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      'Resend code 0 : $_start',
                      style: TEXT_FONT,
                    ),
                  ),
                  CustomButton(
                    buttonColor: isVisible ? WHITE : COLOR_BUTTON_1,
                    iconColor: isVisible ? COLOR_BACKGROUND : WHITE,
                    onPressed: () {
                      BaseAuth.submitOTP(
                              verificationId, _pinEditingController.text)
                          .then((uid) => {
                                BaseAuth.getCurrentFirebaseUser()
                                    .then((user) => {
                                          if (user != null)
                                            {
                                              Navigation.instance
                                                  .navigateAndRemove(
                                                      SIGNUP_ACOUNT,
                                                      arguments: user)
                                            }
                                        })
                              })
                          .catchError((error) {
                        showOverlay('Incorrect Verification Code', context);
                      });
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
