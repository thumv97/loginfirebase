import 'package:acazia_training/navigation/navigation.dart';
import 'package:acazia_training/navigation/paths.dart';
import 'package:acazia_training/res/colors.dart';
import 'package:acazia_training/res/texts.dart';
import 'package:acazia_training/utils/validations.dart';
import 'package:acazia_training/view/widgets/back_button.dart';
import 'package:acazia_training/view/widgets/custom_button.dart';
import 'package:acazia_training/view/widgets/custom_textfield.dart';
import 'package:acazia_training/view/widgets/show_overlay.dart';
import 'package:flutter/material.dart';

class SignUpPhone extends StatefulWidget {
  @override
  _SignUpPhoneState createState() => _SignUpPhoneState();
}

class _SignUpPhoneState extends State<SignUpPhone> {
  bool isVisible = false;
  final _phoneNumberController = TextEditingController();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_BACKGROUND,
      body: Padding(
        padding: MediaQuery.of(context)
            .padding
            .add(const EdgeInsets.symmetric(vertical: 16.0, horizontal: 10.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            backButton(),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 35, horizontal: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const Text(
                      'Create\nAccount',
                      style: TEXT_TITLE,
                    ),
                    const Spacer(),
                    Expanded(
                      child: CustomTextField(
                        controller: _phoneNumberController,
                        inputType: TextInputType.phone,
                        prefixIcon: SizedBox(
                          width: 40,
                          child: Row(
                            children: <Widget>[
                              const Text('VN', style: TEXT_FONT),
                              Icon(
                                Icons.arrow_drop_down,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                        hintText: 'Phone Number',
                        onChanged: (value) {
                          if (!Validations.validatePhoneNumber(value) ||
                              value.toString().isEmpty) {
                            setState(() {
                              isVisible = false;
                            });
                            debugPrint('Invalid');
                          } else {
                            setState(() {
                              isVisible = true;
                            });

                            debugPrint('Correct');
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Sign in',
                    style: TextStyle(
                        fontSize: 16,
                        color: WHITE,
                        decoration: TextDecoration.underline),
                  ),
                  CustomButton(
                    buttonColor: isVisible ? WHITE : COLOR_BUTTON_1,
                    iconColor: isVisible ? COLOR_BACKGROUND : WHITE,
                    onPressed: () {
                      isVisible
                          ? Navigation.instance.navigateTo(SIGNUP_VERIFY,
                              arguments: _phoneNumberController.text)
                          : showOverlay('Invalid Phone Number', context);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
