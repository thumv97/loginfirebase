import 'package:acazia_training/navigation/navigation.dart';
import 'package:acazia_training/navigation/paths.dart';
import 'package:acazia_training/res/colors.dart';
import 'package:acazia_training/res/texts.dart';
import 'package:acazia_training/service/auth.dart';
import 'package:acazia_training/utils/validations.dart';
import 'package:acazia_training/view/widgets/back_button.dart';
import 'package:acazia_training/view/widgets/custom_button.dart';
import 'package:acazia_training/view/widgets/custom_textfield.dart';
import 'package:acazia_training/view/widgets/show_overlay.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final _loginController = TextEditingController();
  bool isVisible = false;
  @override
  void initState() {
    super.initState();
    _checkLogin();
  }

  void _checkLogin() {
    BaseAuth.getCurrentFirebaseUser().then((value) => {
      if (value != null)
        {Navigation.instance.navigateTo(HOME, arguments: value)}
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_BACKGROUND,
      body: Padding(
        padding: MediaQuery.of(context)
            .padding
            .add(const EdgeInsets.symmetric(vertical: 16.0, horizontal: 10.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            backButton(),
            Expanded(
              child: Padding(
                padding:
                const EdgeInsets.fromLTRB(16.0,35.0,16.0,0.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const Text(
                      'Welcome\nBack',
                      style: TEXT_TITLE,
                    ),
                    const Spacer(),
                    Expanded(
                      child: CustomTextField(
                        controller: _loginController,
                        inputType: TextInputType.text,
                        hintText: 'Sign In Email',
                        onChanged: (value) {
                          setState(() {
                            if (!Validations.validatePhoneNumber(value) &&
                                !Validations.validateEmail(value) ||
                                value.toString().isEmpty) {
                              isVisible = false;
                              debugPrint('Invalid');
                            } else {
                              isVisible = true;
                              debugPrint('Correct');
                            }
                          });
                        },
                      ),
                    ),
                    const Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                Navigation.instance
                                    .navigateTo(SIGNUP, arguments: null);
                              },
                              child: Text(
                                'Sign up',
                                style: TextStyle(
                                    fontSize: 16,
                                    color: WHITE,
                                    decoration: TextDecoration.underline),
                              ),
                            ),
                            Text(
                              'Forgot Password',
                              style: TextStyle(
                                  fontSize: 16,
                                  color: WHITE,
                                  decoration: TextDecoration.underline),
                            ),
                          ],
                        ),
                        const SizedBox(height: 16.0,),
                        CustomButton(
                          buttonColor: isVisible ? WHITE : COLOR_BUTTON_1,
                          iconColor: isVisible ? COLOR_BACKGROUND : WHITE,
                          onPressed: () {
                            isVisible
                                ? BaseAuth.signIn(_loginController.text)
                                .then((uid) => {
                              BaseAuth.getCurrentFirebaseUser()
                                  .then((user) => {
                                Navigation.instance
                                    .navigateTo(HOME,
                                    arguments: user)
                              })
                            })
                                : showOverlay('Invalid Email', context);
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
