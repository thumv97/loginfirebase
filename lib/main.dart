import 'package:acazia_training/navigation/navigation.dart';
import 'package:acazia_training/navigation/routes.dart';
import 'package:acazia_training/view/pages/signup/signup_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      navigatorKey: Navigation().navigatorKey,
      onGenerateRoute: generateRoute,
      theme: ThemeData(
        primaryColor: Colors.white,
        primarySwatch: Colors.cyan,
      ),
      home: SignUpPage(),
    );
  }
}


