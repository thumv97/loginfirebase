import 'package:cloud_firestore/cloud_firestore.dart';

class Users {
  String userId;
  String name;
  String email;
  String phone;
  String profilePictureURL;

  Users({
    this.userId,
    this.name,
    this.email,
    this.phone,
    this.profilePictureURL,
  });

  Users.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    profilePictureURL = json['profilePictureURL'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['userId'] = userId;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['profilePictureURL'] = profilePictureURL;
    return data;
  }

  factory Users.fromDocument(DocumentSnapshot doc) {
    return Users.fromJson(doc.data);
  }
}
